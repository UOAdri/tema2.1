const dictionary = ['javascript', 'java', 'python']

/*
functia sanitize primeste ca parametru un text si un dictionar
cuvintele din dictionar sunt inlocuite in text cu prima litera urmata de o serie de asteriscuri (egala cu numarul de litere inlocuite) urmata de ultima litera
e.g. daca 'decembrie' exista in dictionar, va fi inlocuit cu 'd*******e'
*/
function sanitize(text, dictionary){
    var j = 0;
    while(j < dictionary.length){
        var cuvant = dictionary[j];
        for(var i = 1; i < cuvant.length-1; i++){
            cuvant = cuvant.replace(cuvant[i], "*");
        }
        text = text.replace(new RegExp(dictionary[j], 'g'), cuvant);
        j++;
    }
    return text;
}


module.exports.dictionary = dictionary
module.exports.sanitize = sanitize